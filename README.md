## [新零售商城系统](https://www.bgniao.cn)
启山智软新零售是一款基于Spring Cloud+MybatisPlus+XXL-JOB+redis+Vue的前后端分离的商城系统，采用轻量级稳定框架开发，执行效率、扩展性、稳定性值得信赖。
是一款经过生产环境 **反复线上论证** 和 **真实用户数据使用** 的Java新零售商城系统。拥有完整下单流程的完全开源项目，本着百尺竿头更进一步的想法 公司决定将启山智软新零售系统全面面向社会。


### 相关链接
	新零售系统	: https://www.bgniao.cn
	公司官网		: https://73app.cn
        运营负责人wx	: xcxqidiankeji
	负责人电话	: 18967889883

### 项目介绍
		是一款基于Spring Cloud 和 Vue.js的新零售系统

		包含了商品管理、订单管理、运费模板、素材库、小程序直播、规格管理、会员管理、运营管理、内容管理、统计报表、权限管理、设置等模块

		启山智软新零售 针对用户痛点新增了 自定义装修(pc==》小程序) 引导页 自定义专区 直播 物流(发货单)模式 魔方装修组件 
		淘宝商品CSV导入 完善的售后体系 会员+消费返利 数据服务 接入盛付通第三方支付享受低费率提现等大批量功能 使用流程更加简单 页面更加简洁美观。功能更加强大且稳定


### 必读
  :arrow_down: :arrow_down: 

	注：如需深入了解业务功能信息请添加相关 运营负责人微信  wx:xcxqidiankeji。获取测试账号进行体验
		商家  需知 : 本平台提供商家入驻功能。商家可根据自身需求来选择对应版本 以最低的成本拥有自己的新零售商城小程序。我们也将提供最完善的服务
   
        如有公益项目需要使用该新零售系统我司愿提供免费的商家入驻及一切售后服务来奉献一些属于自己的爱心 ：公益项目需提供相关证明,我司将拥有针对该项的最终解释权  


### 项目演示
![](https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/%E5%B0%8F.gif "启山智软社区团购操作流程")

移动端
![](https://images.gitee.com/uploads/images/2021/0702/160747_f27d2e38_8533008.jpeg "移动端展示.jpg")

Web端管理
![](https://images.gitee.com/uploads/images/2021/0702/160736_c22cc63c_8533008.jpeg "11.jpg")

### 小程序码，可联系管理员索要体验权限
![小程序体验码](https://images.gitee.com/uploads/images/2021/0701/092820_d3abefd1_8533008.png "体验码.png")
    


### 技术选型

| 技术                   | 说明                 | 官网                                                 |
| ---------------------- | -------------------- | ---------------------------------------------------- |
| Spring Cloud           | 微服务框架           | https://spring.io/projects/spring-cloud              |
| Spring Cloud Alibaba   | 微服务框架           | https://github.com/alibaba/spring-cloud-alibaba      |
| Spring Boot            | 容器+MVC框架         | https://spring.io/projects/spring-boot               |
| MyBatis-Plus           | 数据层代码生成       | http://www.mybatis.org/generator/index.html          |
| Swagger                | 文档生成工具         | https://swagger.io/     
|                                                                                                     |
| Elasticsearch          | 搜索引擎             | https://github.com/elastic/elasticsearch             |
| RabbitMq               | 消息队列             | https://www.rabbitmq.com/                            |
| Redis                  | 分布式缓存           | https://redis.io/                                    |
| Druid                  | 数据库连接池         | https://github.com/alibaba/druid                     |
| OSS                    | 对象存储             | https://github.com/aliyun/aliyun-oss-java-sdk        |
| JWT                    | JWT登录支持          | https://github.com/jwtk/jjwt                         |
| XXL-JOB                | 分布式任务调度平台   |https://www.xuxueli.com/xxl-job/                       |
|                                                                                                     |
| Lombok                 | 简化对象封装工具     | https://github.com/rzwitserloot/lombok               |
| Jenkins                | 自动化部署工具       | https://github.com/jenkinsci/jenkins                 |
| Docker                 | 应用容器引擎         | https://www.docker.com/                              |          
|Sonarqube				 | 代码质量控制	        |https://www.sonarqube.org/
|                                                                                                     |
| element                | 组件库         | https://element.eleme.cn/#/zh-CN                           |
| Vue.js                 | 渐进式JavaScript 框架       | https://cn.vuejs.org/                         |
| Vue-router 			 | 前端路由 		       | https://router.vuejs.org/zh/ 	       |
| vuex 					 | 状态管理            | https://vuex.vuejs.org/zh/ 		|			
| modeuse-core 			 | 自主开发UI组件       | -- 				                |
| TypeScript             | JavaScript超集       | https://www.tslang.cn/                                |
| eslint             	 | 代码质量控制         | https://eslint.org/                                   |                 
| hook	             	 | 代码质量控制         |                                                       |
| minapp                 |            小程序模板 | https://qiu8310.github.io/minapp/                    |
|--------|-------|-----------------------------------|

                 


### 功能模块
	基础服务：
		oss对象存储 支持 阿里云 腾讯云 七牛云(加速图片读取速度)
		sms短信服务 支持 腾讯云 阿里云   
		支付服务    支持微信支付 余额支付 好友代付 额外对接了盛付通 使得商家提现费率更低 自动分账操作更为方便
		总台服务    控制商户入驻。及各种信息私有配置
	功能服务：
		以下仅列出个别服务。如需深入了解请添加相关负责人微信
		
		商品服务		: 商品展示更加美观 多专区自由切换 操作流程简单 支持淘宝商品CSV导入等
		订单服务		: 支持多样化付款 积分，满减，优惠券，会员，一元换购，秒杀等多样营销方式来增加用户粘合度等。
		直播服务		: 通过小程序直播助手进行对商品的直播，并可获取直播点赞量观看量等
		DIV装修		: 商家可通过自定义装修来根据自己的喜好装修小程序,比市场上的模板更多元化 操作更简单 玩法更多
		数据服务		: 获取商家近期的交易概况(待支付|待发货|待签收|待评论|) 
		 实时概况	: 获取商家指定时间内的实时概况(交易量|交易额|浏览量|佣金)
		 排行榜		: 获取商家当月的排行版信息数据(团长排行榜|供应商排行版|商品排行榜)



## [更新详细说明](https://www.bgniao.cn/notice)


### 商城功能

- 一：商品管理：上传商品、规格sku管理、商品上下架、分类管理等
- 二：订单管理：订单结算、购物车、订单支付、评价、售后等
- 三：物流管理：收发地址管理、物流发货、电子面单打印、小票打印、收货等
- 四：会员管理：会员卡、会员权益、会员管理、储值管理等
- 五：营销模块：优惠券、满减、积分商城、直播、社群接龙、环保回收等
- 六：财务模块：对账、提现审核、报表批量导出
- 七：用户模块：用户管理、自动标签、积分充值等
- 八：会员模块：会员卡、会员权益、会员管理、储值管理等
- 九：自定义装修模块：所有页面DIY自定义装修首页、个人中心页、底部导航栏、商品列表页等
- 十：素材导入：淘宝、天猫、拼多多、京东等电商平台一键导入商品素材
- 十一：商超设置模块：绑定自己的公众号和小程序统一管理、店铺设置、下单设置、打印机连接配置、自动短信或公众号推送设置、支付和返利配置



### B/S 新零售系统商家端功能清单
经营概况

		实时概况		: 根据时间获取该期间内的商家交易情况实时反馈给商家最直观的感受
		交易概况		: 订单相关数据,包含(待付款,待发货,待签收,待提货)等订单相关状态信息 
		排 行 榜		: 展示当月内团长,供应商,商品的排行信息(交易额 or 交易量)
		... ... 	: 更多内容请添加运营人员微信获取商家端操作账号进行体验

		
商品管理

		自定义商品专区: 商品按分区展示,并可以实时调整专区 秒杀 商超 拼团 自定义专区等等随时随意切换 
		产品列表		 : 设置商品投放区域,填充虚拟销量,多规格设置,快速转移专区,购买赠送积分等
		cvs素材导入	 : 素材导入-在淘宝clone一键导入素材,大大减少了商家繁琐的工作量
		... ...		 : 更多内容请添加运营人员微信获取商家端操作账号进行体验
订单管理

		快递订单		: 物流形式订单,使得商家商品不在局限于当前城市,可向省外扩展
		售后工单		: 订单退换货，退款等处理列表
		订单评价管理     : 已购该产品得用户针对该物品评价 打分。商家可进行回复 可将用户评价设置为精选展现到用户小程序端
		... ...		: 更多内容请添加运营人员微信获取商家端操作账号进行体验
营销应用

	   优惠券		: 商家在店铺后台创建发放优惠券(满减|满折|无门槛|折扣) 增加用户黏贴性
	   满  减		: 商家在店铺后台创建满减活动，选择指定商品参加活动，用户下单付款该商品可立减优惠
	   积分商城		: 商家推出积分商品引流，保持客户粘性，带动其他商品的销售。后台可以对积分商品和积分优惠券新增、编辑等操作
	   直  播		: 商家通过微信直播带货，。商家可以根据团长手动控制投放直播间展示
	   商品基金		: 商家可创建基金，用户购买商品后可获得基金，基金可做公益、返利等项目
	   回收返券		: 商家可创建回收功能，用户提交回收物品可返优惠券，例：旧衣回收，以旧换新等
	   ... ...		: 一元换购 秒杀等更多功能玩法请进入后台了解 
财务管理

	   对账单		: 商家实时查看小程序交易流水记录
	   提现工单		: 统一管理平台每个角色的提现审核及提现记录
	   ... ...		: 更多内容请添加运营人员微信获取商家端操作账号进行体验
客户管理

	   客户列表		: 统一管理用户信息 针对客户进行操作,列如充值积分 赠送优惠券升级为会员等等
	   会员管理		: 统一管理会员用户信息 会员开卡形式(付费|积分|累计消费金额|购买指定商品等)
	   ... ...		: 更多内容请添加运营人员微信获取商家端操作账号进行体验
配送方式
	  
	   快递配送		: 设置多元运费模块(包邮|按件收费|按重量收费) 开通对应物流服务查看物流规则等等
	   ... ...		: 更多内容请添加运营人员微信获取商家端操作账号进行体验
商城设置
		
	   交易设置		: 设置订单交易规则(订单设置|售后设置) 用户下单语音播报提醒等
	   支付设置		: 微信支付 (第三方支付)盛付通 减少商家/提货点/团长等角色的提现所需手续费 自动分账操作使用方便
	   小程序/公众号设置	: 自主授权绑定小程序和公众号，手动更新版本，绑定后可获得消息回复，定时群发等功能。
	   消息设置		: 如已绑定公众号和小程序则可配置消息模板，进行自动短信或公众号或小程序模板消息的推送。
	   返利设置		: 配置小程序消费返利功能，用户消费返利至余额，余额当钱用，可增加用户粘性。
	   装修    		: 高自由度自定义装修小程序首页，组件完善。
	   ... ...		: 更多内容请添加运营人员微信获取商家端操作账号进行体验 


## 商家端页面展示
	经营概况 产品列表
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611543033(1).jpg" width="500" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611543676(1).jpg" width="450" height="500">
	
	商品分类 素材导入

<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611543972(1).jpg" width="500" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611543937(1).jpg" width="450" height="500">

	积分商城 直播
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611544698.jpg" width="500" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611544698(1).jpg" width="450" height="500">

	客户列表 会员卡管理
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611544903(1).jpg " width="500" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611544922(1).jpg" width="450" height="500">

	快递配送
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545171(1).jpg " width="500" height="500">

	引导页 交易设置
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545316(1).jpg " width="500" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545363(1).jpg " width="500" height="500">

	自定义装修
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545742(1).jpg " width="1000" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545782(1).jpg " width="300" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545804.jpg " width="300" height="500">
<img src="https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/1611545831(1).jpg " width="300" height="500">

