package com.medusa.basemall.constant;

public interface CouponType {

	/**
	 *满xx减券
	 */
	Integer FULL_REDUCE_COUPON = 1;
	/**
	 *满xx打折券
	 */
	Integer FULL_DISCOUNT_COUPON = 2;
	/**
	 *无门槛现金券
	 */
	Integer READY_MONEY_COUPON = 3;
	/**
	 *无门槛打折券
	 */
	Integer DISCOUNT_COUPON = 4;
}
