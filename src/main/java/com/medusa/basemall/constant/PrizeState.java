package com.medusa.basemall.constant;


/***
 * 积分商品类型
 *
 * @author Created by wx on 2018/09/03.
 */
public interface PrizeState {

    /**
     * 上架状态
     */
    Integer SHELVES = 1;

    /**
     * 下架状态
     */
   Integer FRAME = 2;

    /**
     * 售罄状态
     */
  Integer SELLOUT = 3;
}
