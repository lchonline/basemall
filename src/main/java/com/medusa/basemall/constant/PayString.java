package com.medusa.basemall.constant;

/**
 * @author Created by wx on 2018/09/03.
 */
public interface PayString {

    String RETURNCODE = "returnCode";

    String SUCCESS = "SUCCESS";
}
