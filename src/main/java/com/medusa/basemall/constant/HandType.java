package com.medusa.basemall.constant;

/***
 * 排序操作
 *
 * @author Created by wx on 2018/09/01.
 */

public interface HandType {

    /**
     * 置顶
     */
    Integer TOP = 1;

    /**
     * 上移一位
     */
    Integer UP = 2;

    /**
     * 下移一位
     */
    Integer DOWN = 3;

    /**
     * 置底
     */
    Integer FOOT = 4;

}
