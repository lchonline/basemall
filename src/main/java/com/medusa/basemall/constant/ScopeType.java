package com.medusa.basemall.constant;

public interface ScopeType {

	/**
	 *从方法参数中取值
	 */
	int ARGUMENTS = 1;

	/**
	 *从方法参数对象中取值
	 */
	int OBJECT = 2;
}
