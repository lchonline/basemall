package com.medusa.basemall.order.service;

import com.medusa.basemall.core.Service;
import com.medusa.basemall.order.entity.OrderRefound;

/**
 * Created by medusa on 2018/06/02.
 */
public interface OrderRefoundService extends Service<OrderRefound> {


}
