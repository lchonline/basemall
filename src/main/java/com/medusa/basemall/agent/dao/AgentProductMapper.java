package com.medusa.basemall.agent.dao;

import com.medusa.basemall.agent.entity.AgentProduct;
import com.medusa.basemall.core.Mapper;

public interface AgentProductMapper extends Mapper<AgentProduct> {
}