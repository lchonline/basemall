package com.medusa.basemall.user.dao;

import com.medusa.basemall.core.Mapper;
import com.medusa.basemall.user.entity.MemberGroup;

public interface MemberGroupMapper extends Mapper<MemberGroup> {
}