package com.medusa.basemall.user.dao;

import com.medusa.basemall.core.Mapper;
import com.medusa.basemall.user.entity.MemberRechargeRecord;

public interface MemberRechargeRecordMapper extends Mapper<MemberRechargeRecord> {
}