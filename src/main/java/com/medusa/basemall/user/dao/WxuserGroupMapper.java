package com.medusa.basemall.user.dao;

import com.medusa.basemall.core.Mapper;
import com.medusa.basemall.user.entity.WxuserGroup;

public interface WxuserGroupMapper extends Mapper<WxuserGroup> {
}