package com.medusa.basemall.product.dao;

import com.medusa.basemall.core.Mapper;
import com.medusa.basemall.product.entity.Wlcompany;

public interface WlcompanyMapper extends Mapper<Wlcompany> {
}