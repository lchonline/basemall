package com.medusa.basemall.product.dao;

import com.medusa.basemall.core.Mapper;
import com.medusa.basemall.product.entity.LogisticDistrobution;

public interface LogisticDistrobutionMapper extends Mapper<LogisticDistrobution> {
}