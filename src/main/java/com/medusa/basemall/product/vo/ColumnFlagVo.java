package com.medusa.basemall.product.vo;

import lombok.Data;

@Data
public class ColumnFlagVo {

	private String appmodelId;

	private Boolean flag;
}
